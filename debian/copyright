Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ggee
Upstream-Contact: IOhannes m zmölnig <umlaeute@debian.org>
Source: https://github.com/pd-externals/ggee

Files: *
Copyright: 1999-2006, Guenter Geiger <geiger@xdv.org>
           2002, Oswald Berthold <opt@web.fm>
           2004-2010, Hans-Christoph Steiner
           2005, Dieter <dieter@rhiz.org>
           2008, Russell Bryant
           2015-2021, Roman Haefeli <reduzent@gmail.com>
License: tcltk

Files: pd-lib-builder/*
Copyright: n/a
License: public-domain

Files: debian/*
Copyright: 2010-2012, Hans-Christoph Steiner
           2010-2011, Felipe Sateler
           2015-2022, IOhannes m zmölnig
License: tcltk

License: tcltk
 This software is copyrighted by Guenter Geiger and others.  The following
 terms apply to all files associated with the software unless explicitly
 disclaimed in individual files.
 .
 The authors hereby grant permission to use, copy, modify, distribute,
 and license this software and its documentation for any purpose, provided
 that existing copyright notices are retained in all copies and that this
 notice is included verbatim in any distributions. No written agreement,
 license, or royalty fee is required for any of the authorized uses.
 Modifications to this software may be copyrighted by their authors
 and need not follow the licensing terms described here, provided that
 the new terms are clearly indicated on the first page of each file where
 they apply.
 .
 IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
 ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
 DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 .
 THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
 IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
 NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
 MODIFICATIONS.
 .
 RESTRICTED RIGHTS: Use, duplication or disclosure by the government
 is subject to the restrictions as set forth in subparagraph (c) (1) (ii)
 of the Rights in Technical Data and Computer Software Clause as DFARS
 252.227-7013 and FAR 52.227-19.

License: public-domain
 Written by Katja Vetter March-June 2015 for the public domain. No warranties.
